import { Handler } from "@netlify/functions";

const headers = {
  "PRIVATE-TOKEN": process.env.GITLAB_PAT,
};

// https://docs.gitlab.com/ee/api/repository_files.html#get-file-from-repository
// https://docs.gitlab.com/ee/api/rest/index.html#how-to-use-the-api
const PROJECT_ID = 43596907;
const BASE_URL = `https://gitlab.com/api/v4/projects/${PROJECT_ID}/repository/files`;
const REF = "b80e873f9651a586b0d799c2fb213c5a8d4ddc2b";
const FILE_PATH = encodeURIComponent("quasar.json");

const DATA_URL = `${BASE_URL}/${FILE_PATH}/raw?ref=${REF}`;

// https://docs.netlify.com/functions/build/?fn-language=ts
// https://github.com/learnwithjason/serverless-typescript/blob/main/netlify/functions/hello-typescript/hello-typescript.ts
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error/toString
const handler: Handler = async () => {
  try {
    const response = await fetch(DATA_URL, { headers });
    // console.log(response);
    const data = await response.json();
    // console.log(data);

    return {
      statusCode: 200,
      body: JSON.stringify({ data }),
    };
  } catch (error) {
    return { statusCode: 500, body: error.toString() };
  }
};

export { handler };
