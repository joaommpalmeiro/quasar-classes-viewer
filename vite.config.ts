import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import isWsl from "is-wsl";

const usePolling = isWsl;
if (usePolling) console.log("Running on WSL…");

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  server: {
    // Once Netlify Dev is used:
    open: false,
    // https://vitejs.dev/config/server-options.html#server-watch
    // For WSL/Windows:
    watch: {
      usePolling,
    },
  },
});
