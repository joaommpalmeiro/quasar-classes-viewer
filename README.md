# quasar-classes-viewer

> https://quasar-classes-viewer.netlify.app/

## Development

```bash
nvm install && nvm use && node --version
```

or

```bash
nvm use && node --version
```

Enable [Volar's Takeover Mode](https://vuejs.org/guide/typescript/overview.html#volar-takeover-mode).

Install [Vue Devtools](https://devtools.vuejs.org/) (if needed).

```bash
npm install -g netlify-cli
```

```bash
npm install
```

```bash
netlify dev
```

or

```bash
netlify functions:serve
```

## Deployment

```bash
npm install
```

```bash
npm run build
```

```bash
netlify env:import .env
```

```bash
netlify deploy --prod
```

## References

- https://explorers.netlify.com/learn/up-and-running-with-serverless-functions
- https://www.netlify.com/blog/how-to-deploy-vue-3-and-vite-app-in-5-minutes/ + https://github.com/bencodezen/vue-3-vite-netlify-template
- https://github.com/vitejs/vite/issues/2571#issuecomment-1152572466
- https://docs.netlify.com/functions/build/?fn-language=ts
- https://docs.netlify.com/functions/optional-configuration/?fn-language=ts#node-js-version-for-runtime
- https://docs.netlify.com/cli/get-started/
- https://docs.netlify.com/configure-builds/troubleshooting-tips/#build-fails-with-exit-status-128
- https://docs.netlify.com/cli/get-started/#manual-deploys
- https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token
- https://zagjs.com/components/vue-sfc/toast
- https://vuejs.org/guide/components/provide-inject.html
- https://github.com/wobsoriano/pinia-xstate

## Notes

- https://github.com/byoungd/modern-vue-template#type-imports (`types.ts` file)
- https://github.com/huibizhang/vitawind
- https://github.com/vuejs/tsconfig
- `npm create vite@latest quasar-classes-viewer -- --template vue-ts`
- https://gitlab.com/joaommpalmeiro/quasar-css
- `@id:vscode.typescript-language-features`
- `npm install vue && npm install -D @vitejs/plugin-vue typescript vite vue-tsc tailwindcss postcss autoprefixer`
- `npx tailwindcss init -p`
- https://tailwindcss.com/docs/guides/vite#vue
- `npm install -D is-wsl`
- https://windstatic.com/
- https://rauno.me/interfaces
- `npm install pinia`
- https://vitejs.dev/guide/env-and-mode.html#env-files
- [Netlify CLI](https://docs.netlify.com/cli/get-started/):
  - `npm install -g netlify-cli`
  - `netlify --version`
  - `netlify login`
  - `netlify init`
- `npm install -D @netlify/functions @types/node`
- `AWS_LAMBDA_JS_RUNTIME` env var: `nodejs18.x` ([source](https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html))
- `cp .env.example .env` + `netlify env:import .env` ([source](https://docs.netlify.com/environment-variables/get-started/#site-environment-variables))
- `lsof -i:8888` + `kill -9 $(lsof -t -i:8888)` ([source](https://stackoverflow.com/questions/11583562/how-to-kill-a-process-running-on-particular-port-in-linux))
- https://docs.gitlab.com/ee/api/repository_files.html:
  - Scope: `read_repository`
- [The Future of Chakra UI (with Segun Adebayo)](https://www.youtube.com/live/I5xEc9t-HZg?feature=share):
  - [Tachyons](https://tachyons.io/)
  - [Styled System](https://styled-system.com/)
  - [XState](https://xstate.js.org/docs/), [@xstate/vue](https://github.com/statelyai/xstate/tree/main/packages/xstate-vue), and [@xstate/fsm](https://xstate.js.org/docs/packages/xstate-fsm/)
  - The name "Chakra UI" is inspired by Naruto
  - State Machines: "Model once, use anywhere"
  - [OpenChakra](https://github.com/premieroctet/openchakra)
  - https://vue.chakra-ui.com/
  - [Chakra UI Figma Kit](https://www.figma.com/community/file/971408767069651759)
  - [Javier Alaves](https://www.figma.com/@javi)
- [XState Visualizer](https://stately.ai/viz)
- https://developer.mozilla.org/en-US/docs/Web/API/Element/blur_event + https://developer.mozilla.org/en-US/docs/Web/API/Element/paste_event
- https://github.com/segunadebayo/egghead-zagjs-course
- https://developer.mozilla.org/en-US/docs/Web/API/FormData
- `npm install @zag-js/toast @zag-js/vue`
- `npm install notiwind`
- `npm install @heroicons/vue @vueuse/core`
- `npm install clsx tailwind-merge`

## Snippets

### Store ([source](https://github.com/pineviewlabs/middlemarch/blob/main/src/store/catalog.js))

```js
import { defineStore } from "pinia";

export const useCatalog = defineStore("catalog-store", {
  state: () => {
    return {
      newArrivals: [],
      fetching: false,
    };
  },

  getters: {
    results(state) {
      return state.newArrivals;
    },

    isFetching(state) {
      return state.fetching;
    },
  },

  actions: {
    async fetchNewArrivals() {
      this.fetching = true;

      const response = await fetch("/data/new-arrivals.json");
      try {
        const result = await response.json();
        this.newArrivals = result.books;
      } catch (err) {
        this.newArrivals = [];
        console.error(err);
        return err;
      }

      this.fetching = false;
    },
  },
});
```
