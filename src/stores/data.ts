import { defineStore } from "pinia";

interface Token {
  class: string;
  properties: string;
}

interface Section {
  name: string;
  classes?: Token[];
}

// https://www.typescriptlang.org/docs/handbook/utility-types.html#returntypetype
interface State {
  quasar: Section[];
  // toast: ReturnType<typeof group.connect> | null;
}

// https://pinia.vuejs.org/core-concepts/#defining-a-store
// https://pinia.vuejs.org/core-concepts/#option-stores
// https://pinia.vuejs.org/core-concepts/state.html
// https://pinia.vuejs.org/core-concepts/state.html#typescript
// https://pinia.vuejs.org/core-concepts/actions.html
// https://dmitripavlutin.com/javascript-fetch-async-await/
// https://github.com/vueschool/pinia-the-enjoyable-vue-store/blob/main/src/stores/ProductStore.js
export const useDataStore = defineStore("data", {
  state: (): State => {
    return {
      quasar: [],
    };
  },
  actions: {
    async fetchData(): Promise<void> {
      // https://stackoverflow.com/questions/60112151/unable-to-fetch-data-from-a-gitlab-file-getting-cors-exception
      // https://github.com/pineviewlabs/middlemarch/blob/main/src/store/catalog.js
      // https://github.com/pineviewlabs/middlemarch/tree/main/public/data
      try {
        const response = await fetch("/.netlify/functions/data");
        const { data } = await response.json();

        this.quasar = data;
      } catch (error) {
        console.error(error);
      }
    },
  },
});
