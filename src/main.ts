import Notifications from "notiwind";
import { createPinia } from "pinia";
import { createApp } from "vue";
import App from "./App.vue";
import "./style.css";

// https://pinia.vuejs.org/getting-started.html
// https://github.com/vueschool/pinia-the-enjoyable-vue-store/blob/main/src/main.js
const pinia = createPinia();
const app = createApp(App);

app.use(pinia);
// https://github.com/emmanuelsw/notiwind
app.use(Notifications);
app.mount("#app");
