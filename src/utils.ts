// Source:
// - https://readwise.io/reader/shared/01gpvn7ww2npxq26f16bphdekr
// - https://twitter.com/shadcn/status/1614692419039105024

import { ClassValue, clsx } from "clsx";
import { twMerge } from "tailwind-merge";

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}
